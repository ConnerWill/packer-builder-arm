<div align="center">
 <span>
  <kbd>
   <a href="https://connerwill.com"><kbd>→ <kbd> π </kbd></kbd></a>
   <a href="https://github.com/ConnerWill"><kbd> GitHub </kbd></a>
   <a href="https://gitlab.com/ConnerWill"><kbd> GitLab </kbd></a>
   <a href="https://github.com/ConnerWill?tab=repositories"><kbd> Repositories </kbd></a>
   <a href="https://gist.github.com/ConnerWill"><kbd> Gists </kbd></a>
   <a href="https://dampsock.com"><kbd> dampsock.com </kbd></a>
   <a href="https://connerwill.com"><kbd> connerwill.com </kbd></a>
   <a href="https://connerwill.com"><kbd><kbd> &infin; </kbd>  </kbd></a>
  </kbd>
 </span>
</div>
  <hr>
<div align="center">
  <img width="480" height="320" src="assets/packer-builder-arm_demo.gif">

# **ｐａｃｋｅｒ－ｂｕｉｌｄｅｒ－ａｒｍ**

> *[*Packer Builder ARM*](https://github.com/ConnerWill/packer-builder-arm)*

[![Build Status][travis-badge]][travis]
[![GoDoc][godoc-badge]][godoc]
[![GoReportCard][report-badge]][report]
[![Docker Cloud Build Status][docker-cloud-build-status]][docker-hub]
[![Docker Pulls][docker-pulls]][docker-hub]
[![Docker Image Size][docker-size]][docker-hub]
![GitHub top language][github-top-language]
![GitHub language count][github-language-count]
![GitHub last commit][github-last-commit]
![GitHub issues][github-issues]
![GitHub repo size][github-repo-size]
[![GitLab][gitlab-badge]][gitlab]
![License][license]
![GitHub Repo stars][github-repo-stars]

  <hr>
</div>


# Overview

## Description

This plugin allows you to build or extend ARM system image. It operates in two modes:
* new - creates empty disk image and populates the *rootfs* on it
* reuse - uses already existing image as the base
* resize - uses already existing image but resize given partition *(i.e. root)*

Plugin mimics standard image creation process, such as:
* builing base empty image (dd)
* partitioning (sgdisk / sfdisk)
* filesystem creation (mkfs.type)
* partition mapping (losetup)
* filesystem mount (mount)
* populate rootfs (tar/unzip/xz etc)
* setup qemu + chroot
* customize installation within chroot

## Demo

<div align="center">

| <a href="assets/packer-builder-arm_demo.gif"><img src="assets/packer-builder-arm_demo.gif" width="720"/></a> | <a href="https://asciinema.org/a/14?autoplay=1"><img src="https://asciinema.org/a/14.png" width="720"/></a> |
|--------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|
|                                                                                                              |                                                                                                             |


</div>


The virtualization works via [*binfmt_misc*](https://en.wikipedia.org/wiki/Binfmt_misc) kernel feature and qemu.

Since the setup varies a lot for different hardware types, the example configuration is available per "board". Currently the following boards are supported (feel free to add more):
* bananapi-r1 (Archlinux ARM)
* beaglebone-black (Archlinux ARM, Debian)
* jetson-nano (Ubuntu)
* odroid-u3 (Archlinux ARM)
* odroid-xu4 (Archlinux ARM, Ubuntu)
* parallella (Ubuntu)
* raspberry-pi (Archlinux ARM, Raspbian)
* raspberry-pi-3 (Archlinux ARM (armv8))
* raspberry-pi-4 (Archlinux ARM (armv7), Ubuntu 20.04 LTS))
* wandboard (Archlinux ARM)
* armv7 generic (Alpine, Archlinux ARM)


# Table Of Contents

<!--ts-->
* [ｐａｃｋｅｒ－ｂｕｉｌｄｅｒ－ａｒｍ](#ｐａｃｋｅｒｂｕｉｌｄｅｒａｒｍ)
* [Packer builder ARM](#)
* [Overview](#overview)
  * [Description](#description)
  * [Demo](#demo)
* [Table Of Contents](#table-of-contents)
* [Quick start](#quick-start)
  * [Run in Docker](#run-in-docker)
    * [Usage via container from Docker Hub:](#usage-via-container-from-docker-hub)
    * [Usage via local container build (supports amd64/aarch64 hosts):](#usage-via-local-container-build-supports-amd64aarch64-hosts)
* [Dependencies](#dependencies)
* [Configuration](#configuration)
  * [Remote file](#remote-file)
  * [Image config](#image-config)
  * [Qemu config](#qemu-config)
* [Chroot provisioner](#chroot-provisioner)
* [Flashing](#flashing)
* [Other](#other)
  * [Generating rootfs archive](#generating-rootfs-archive)
  * [Resizing image](#resizing-image)
  * [Docker](#docker)
  * [CI/CD](#cicd)
  * [How is this plugin different from solo-io/packer-builder-arm-image](#how-is-this-plugin-different-from-solo-iopacker-builder-arm-image)
* [Examples](#examples)
* [Troubleshooting](#troubleshooting)


<!-- Added by: ConnerWill, at: Sat Jul 30 09:47:00 AM CDT 2022 -->

<!--te-->

# Quick start


```console
git clone https://github.com/connerwill/packer-builder-arm
cd packer-builder-arm
go mod download
go build

sudo packer build boards/odroid-u3/archlinuxarm.json
```

## Run in Docker

This method is useful for *macOS* users where is no native way to use *qemu-user-static* *(or Linux users, who do not want to setup packer and all the tools).*


### Docker Hub Container


```console
docker run --rm --privileged -v /dev:/dev -v ${PWD}:/build mkaczanowski/packer-builder-arm build boards/raspberry-pi/raspbian.json
```

More system packages *(e.g. bmap-tools, zstd)* can be added via the parameter `-extra-system-packages=...`:


```console
docker run --rm --privileged -v /dev:/dev -v ${PWD}:/build mkaczanowski/packer-builder-arm build boards/raspberry-pi/raspbian.json -extra-system-packages=bmap-tools,zstd
```

### Local docker container build

Usage via local container build (supports amd64/aarch64 hosts):

> **Build the container locally:**

```console
docker build -t packer-builder-arm -f docker/Dockerfile .
```

> **Run packer via the local built container:**

```console
docker run --rm --privileged -v /dev:/dev -v ${PWD}:/build packer-builder-arm build boards/raspberry-pi/raspbian.json
```

<p align="right">⦗ <a href="#top">𝔟𝔞𝔠𝔨 𝔱𝔬 𝔱𝔬𝔭 ⤒</a> ⦘</p>

# Dependencies

* `sfdisk / sgdisk`
* `e2fsprogs`
* `parted` (resize mode)
* `resize2fs` (resize mode)
* `qemu-img` (resize mode)

# Configuration

The configuration is split into 3 parts:
* remote file config
* image config
* qemu config

## Remote file

Describes the remote file that is going to be used as base image or *rootfs* archive (depending on `image_build_method`)

```json
"file_urls" : ["http://os.archlinuxarm.org/os/ArchLinuxARM-odroid-xu3-latest.tar.gz"],
"file_checksum_url": "http://hu.mirror.archlinuxarm.org/os/ArchLinuxARM-odroid-xu3-latest.tar.gz.md5",
"file_checksum_type": "md5",
"file_unarchive_cmd": ["bsdtar", "-xpf", "$ARCHIVE_PATH", "-C", "$MOUNTPOINT"],
"file_target_extension": "tar.gz",
```

The `file_unarchive_cmd` is optional and should be used if the standard golang archiver can't handle the archive format.

Raw images format (`.img` or `.iso`) can be used by defining the `file_target_extension` appropriately.

## Image config

The base image description (size, partitions, mountpoints etc).

```json
"image_build_method": "new",
"image_path": "odroid-xu4.img",
"image_size": "2G",
"image_type": "dos",
"image_partitions": [
    {
        "name": "root",
        "type": "8300",
        "start_sector": "4096",
        "filesystem": "ext4",
        "size": "0",
        "mountpoint": "/"
    }
],
```

The plugin doesn't try to detect the image partitions because that varies a lot.
Instead it solely depend on `image_partitions` specification, so you should set that even if you reuse the image (`method` = reuse).

## Qemu config

Anything *qemu* related:

```json
"qemu_binary_source_path": "/usr/bin/qemu-arm-static",
"qemu_binary_destination_path": "/usr/bin/qemu-arm-static"
```

# Chroot provisioner
To execute command within *chroot* environment you should use *chroot communicator*:

```json
"provisioners": [
 {
   "type": "shell",
   "inline": [
     "pacman-key --init",
     "pacman-key --populate archlinuxarm"
   ]
 }
]
```

This plugin doesn't resize partitions on the base image. However, you can easily expand partition size at the boot time with a systemd service. [Here](./boards/raspberry-pi/archlinuxarm.json) you can find real-life example, where a raspberry pi root-fs partition expands to all available space on sdcard.

# Flashing

To dump image on device you can use [custom postprocessor](https://github.com/connerwill/packer-post-processor-flasher) (really wrapper around `dd` with some sanity checks):

```json
"post-processors": [
 {
     "type": "flasher",
     "device": "/dev/sdX",
     "block_size": "4096",
     "interactive": true
 }
]   
```

<p align="right">⦗ <a href="#top">𝔟𝔞𝔠𝔨 𝔱𝔬 𝔱𝔬𝔭 ⤒</a> ⦘</p>

# Other

## Generating rootfs archive

While image (`.img`) format is useful for most cases, you might want to use
rootfs for other purposes (ex. export to docker). This is how you can generate
rootfs archive instead of image:

```json
"image_path": "odroid-xu4.img" # generates image
"image_path": "odroid-xu4.img.tar.gz" # generates rootfs archive
```

## Resizing image

Currently resizing is only limited to expanding single `ext{2,3,4}` partition with `resize2fs`.

This is an often requested feature where an already built image is given and, we need to expand the main partition to accommodate changes made in provisioner step *(i.e. installing packages).*

To resize a partition you need to set `image_build_method` to `resize` mode and set selected partition size to `0`. 

For example:

```json
"builders": [
  {
    "type": "arm",
    "image_build_method": "resize",
    "image_partitions": [
      {
        "name": "boot",
        ...
      },
      {
        "name": "root",
        "size": "0",
        ...
      }
    ],
    ...
  }
]
```

Complete examples:

- [`boards/raspberry-pi/raspbian-resize.json`](./boards/raspberry-pi/raspbian-resize.json)
- [`boards/beaglebone-black/ubuntu.hcl`](./boards/beaglebone-black/ubuntu.hcl)

## Docker

With `artifice` plugin you can pass *rootfs* archive to docker plugins

```json
"post-processors": [
    [{
        "type": "artifice",
        "files": ["rootfs.tar.gz"]
    },
    {
        "type": "docker-import",
        "repository": "mkaczanowski/archlinuxarm",
        "tag": "latest"
    }],
    ...
]
```

## CI/CD

This is the live example on how to use github actions to push image to docker image registry:

```console
cat .github/workflows/archlinuxarm-armv7-docker.yml
```

> How is this plugin different from `solo-io/packer-builder-arm-image`
> 
> > https://github.com/hashicorp/packer/pull/8462

# Examples

For more examples please see:

```console
tree boards/
```

# Troubleshooting

Many of the reported issues are platform/OS specific. If you happen to have
problems, the first question you should ask yourself is:

> Is my setup faulty? or is there an actual issue?

To answer that question, I'd recommend reproducing the error on the VM, for
instance:


```console
cd packer-builder-arm
vagrant up
vagrant provision
```

> Note: For this the disksize plugin is needed if not already installed `vagrant plugin install vagrant-disksize`


<p align="right">⦗ <a href="#top">𝔟𝔞𝔠𝔨 𝔱𝔬 𝔱𝔬𝔭 ⤒</a> ⦘</p>
<div align="center">
 <span>
  <hr>
  <br>
  <kbd>
   <a href="https://connerwill.com"><kbd>→ <kbd> π </kbd></kbd></a>
   <a href="https://github.com/ConnerWill"><kbd> GitHub </kbd></a>
   <a href="https://gitlab.com/ConnerWill"><kbd> GitLab </kbd></a>
   <a href="https://github.com/ConnerWill?tab=repositories"><kbd> Repositories </kbd></a>
   <a href="https://gist.github.com/ConnerWill"><kbd> Gists </kbd></a>
   <a href="https://dampsock.com"><kbd> dampsock.com </kbd></a>
   <a href="https://connerwill.com"><kbd> connerwill.com </kbd></a>
   <a href="https://connerwill.com"><kbd><kbd> &infin; </kbd> ← </kbd></a>
  </kbd>
 </span>
</div>

[github-top-language]: https://img.shields.io/github/languages/top/ConnerWill/packer-builder-arm
[github-language-count]: https://img.shields.io/github/languages/count/ConnerWill/packer-builder-arm
[github-last-commit]: https://img.shields.io/github/last-commit/ConnerWill/packer-builder-arm
[github-issues]: https://img.shields.io/github/issues-raw/ConnerWill/packer-builder-arm
[github-repo-size]: https://img.shields.io/github/repo-size/ConnerWill/packer-builder-arm
[gitlab-badge]: https://img.shields.io/static/v1?label=gitlab&logo=gitlab&color=E24329&message=mirrored
[gitlab]: https://gitlab.com/ConnerWill/packer-builder-arm
[license]: https://img.shields.io/github/license/ConnerWill/packer-builder-arm
[github-repo-stars]: https://img.shields.io/github/stars/ConnerWill/packer-builder-arm?style=social

[travis-badge]: https://app.travis-ci.com/ConnerWill/packer-builder-arm.svg?branch=master
[travis]: https://app.travis-ci.com/ConnerWill/packer-builder-arm/
[godoc-badge]: https://godoc.org/github.com/connerwill/packer-builder-arm?status.svg
[godoc]: https://godoc.org/github.com/connerwill/packer-builder-arm
[report-badge]: https://goreportcard.com/badge/github.com/connerwill/packer-builder-arm
[report]: https://goreportcard.com/report/github.com/connerwill/packer-builder-arm
[docker-hub]: https://hub.docker.com/r/rl9uu6smkj/packer-builder-arm
[docker-cloud-build-status]: https://img.shields.io/docker/cloud/build/rl9uu6smkj/packer-builder-arm
[docker-pulls]: https://img.shields.io/docker/pulls/rl9uu6smkj/packer-builder-arm
[docker-size]: https://img.shields.io/docker/image-size/rl9uu6smkj/packer-builder-arm
