pacman-key --init
pacman-key --populate archlinuxarm
pacman -Syu

pacman -Sv --noconfirm --needed \
  dosfstools \
  btrfs-progs \
  rsync \
  unzip \
  base-devel \
  uboot-tools \
  mkinitcpio-utils \
  mkinitcpio-netconf \
  mkinitcpio-dropbear
