#!/usr/bin/env bash
#shellcheck disable=2128
###############################################
## Make sure to have updated pacman keyring and
## then cache before installing packages.
##
## ```shell
##    pacman-key --init
##    pacman-key --populate archlinuxarm
## ```
###############################################

arch_user="${arch_user:-alarm}"
yay_download_dir="${HOME:-/home/${arch_user}}/.local/opt"
yay_git_url="git clone https://aur.archlinux.org/yay.git"

if [[ ! -d "$yay_download_dir" ]]; then
  mkdir -pv "$yay_download_dir" ||
    printf "\e[0;1;38;5;196mError creating yay installation dir %s\e[0;1;38;5;196m" "$yay_download_dir" ||
    return 1
fi

cd "$yay_download_dir" ||
  printf "\e[0;1;38;5;196mError cd into yay parent dir %s\e[0m" "$yay_download_dir" ||
  return 1

declare -a install_pkgs_array

## Array of packages to install
install_pkgs_array=(
  base-devel
  git
)

## Sync the package cache (in case it hasnt been done)
printf "\e[0;1;38;5;33mSyncing pacman cache\e[0m\n"
pacman -Syv &&
  printf "\e[0;1;38;5;46mSyncing pacman cache\e[0m\n" ||
  printf "\e[0;1;38;5;196mError syncing pacman cache\e[0m\n" ||
  exit 1

## Install packages
printf "\e[0;1;38;5;33mInstalling packages\e[0m\n"
pacman -Sv \
  --noconfirm \
  --needed \
  "$install_pkgs_array" ||
  printf "\e[0;1;38;5;196mError installing '%s'\e[0m\n" "$install_pkgs_array" ||
  exit 1

git clone -v "$yay_git_url" "$yay_download_dir/yay" ||
  printf "\e[0;1;38;5;196mError cloning into dir %s\e[0m" "$yay_download_dir" ||
  return 1

"$yay_download_dir/yay" ||
  printf "\e[0;1;38;5;196mError cd into yay installation dir %s\e[0m" "$yay_download_dir" ||
  return 1

makepkg -si ||
  printf "\e[0;1;38;5;196mError running makepkg\e[0m" ||
  return 1

if command -v yay; then
  printf "\e[0;1;38;5;46mVerified yay can be found by %s\e[0m\n" "$arch_user"
  return 0
else
  printf "\e[0;1;38;5;196mCannot find yay in path\e[0m\n"
  exit 1
fi
