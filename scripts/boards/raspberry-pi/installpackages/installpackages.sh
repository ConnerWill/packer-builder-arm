#!/usr/bin/env bash
#shellcheck disable=2128
###############################################
## Make sure to have updated pacman keyring and
## then cache before installing packages.
##
## ```shell
##    pacman-key --init
##    pacman-key --populate archlinuxarm
## ```
###############################################
declare -a install_pkgs_array

## Array of packages to install
install_pkgs_array=(
  rsync
  base-devel
  zsh
  neovim
  tmux
  git
)

## Sync the package cache (in case it hasnt been done)
printf "\e[0;1;38;5;33mSyncing pacman cache\e[0m\n"
pacman -Syv &&
  printf "\e[0;1;38;5;46mSyncing pacman cache\e[0m\n" ||
  printf "\e[0;1;38;5;196mError syncing pacman cache\e[0m\n" ||
  exit 1

## Install packages
pacman -Sv \
  --noconfirm \
  --needed \
  "$install_pkgs_array" ||
  printf "\e[38;5;196mError installing '%s'\e[0m\n" "$install_pkgs_array" ||
  exit 1
