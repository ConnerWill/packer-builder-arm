package main

import (
	"github.com/connerwill/packer-builder-arm/builder"
	"github.com/hashicorp/packer-plugin-sdk/plugin"
)

func main() {
	server, err := plugin.Server()
	if err != nil {
		panic(err)
	}
	if err := server.RegisterBuilder(builder.NewBuilder()); err != nil {
		panic(err)
	}
	server.Serve()
}
